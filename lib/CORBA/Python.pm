use strict;
use warnings;

package CORBA::Python;

our $VERSION = '2.63';

use CORBA::Python::NameVisitor;
use CORBA::Python::ImportVisitor;
use CORBA::Python::LiteralVisitor;
use CORBA::Python::ClassVisitor;

1;

