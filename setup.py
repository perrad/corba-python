from distutils.core import setup

setup(
    name='PyIDL',
    version='2.63',
    description='CORBA-IDL runtime',
    author='Francois Perrad',
    author_email='francois.perrad@gadz.org',
    license='Artistic',
    packages=['PyIDL'],
)

